import java.util.Scanner;

public class Booking {
    static String[] cinema = { "Cinema1", "Cinema2", "Cinema3", "Cinema4" };
    static String[] movie = { "rose", "jennie", "jisoo", "lisa" };
    static String[][] seat = {
            { "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10" },
            { "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10" },
            { "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10" }
    };
    static String[] seatvip = { "P1", "P2", "P3", "P4", "P5", "P6" };
    static int priceseat = 0;
    private static String pay;
    private static String member;

    public static void main(String[] args) {   //mian
        System.out.println("Welcome to our theater");
        String name, mail, pay, tel;
        int numberMovie = 0, numberCenima = 0, unitseat = 0;
        Scanner kb = new Scanner(System.in);

        System.out.print("Please input your name / phone number / email : ");//profile
        name = kb.next();
        mail = kb.next();
        tel = kb.next();
        
        System.out.println("Input 1 to see the list of available movies, or input 2 to see the list of cinemas.");
        int selection = kb.nextInt();
        
        if (selection == 1) {
            
        displayMovies();
        System.out.print("Input the number of the movie: ");//number of the movie
        numberMovie = kb.nextInt() - 1;
        System.out.println("Movie: " + movie[numberMovie] + "\nDuration: 2 hr. Available in:");

        displayCinemas();
        System.out.print("Input the number of the cinema: ");//number of the cinema
        numberCenima = kb.nextInt() - 1;
        System.out.print("Input the number of seats wanted: ");//number of seats
        unitseat = kb.nextInt();
        
        }else if (selection == 2){
            
        displayCinemas();
        System.out.print("Input the number of the cinema: ");//number of the cinema
        numberCenima = kb.nextInt() - 1;
        System.out.print("Input the number of seats wanted: ");//number of seats
        unitseat = kb.nextInt();
        
        displayMovies();
        System.out.print("Input the number of the movie: ");//number of the movie
        numberMovie = kb.nextInt() - 1;
        System.out.println("Movie: " + movie[numberMovie] + "\nDuration: 2 hr. Available in:");
        }

        displaySeatingArrangement();
        System.out.print("Input seat IDs: ");//seat IDs
        String[] IDseat = new String[unitseat];
        for (int i = 0; i < unitseat; i++) {
            IDseat[i] = kb.next();
        }

        bookSeats(IDseat);
        displaySeatingArrangement();
        System.out.print("Payment options: 1. Cash, 2. Mobile banking: "); //pay
        pay = kb.next();
        
        System.out.print("Are you a member? (Yes/No): ");
        String member = kb.nextLine();
        if (member.equalsIgnoreCase("Yes")) {
                //IDseat = (IDseat*100)/10;
        }
        
        System.out.println("---------------------------------");
        System.out.println("Your tickets is");//tickets
        printTicket(IDseat, name, tel, mail,numberCenima,numberMovie);
        System.out.println("---------------------------------");
        System.out.print("Confirm (yes/no): ");
        kb.nextLine();
        String confirmation = kb.nextLine();
        if (confirmation.equalsIgnoreCase("Yes")) {
                // Process the booking...
                System.out.println("Booking confirmed. Enjoy the movie!");
        } else {
                System.out.println("Booking canceled.");
        }
    }

    static void displayMovies() { //Movie
        System.out.print("Movie options:\n");
        for (int i = 0; i < movie.length; i++) {
            System.out.println((i + 1) + ". " + movie[i]);
        }
    }

    static void displayCinemas() { //Cinema
        System.out.println("Cinema options:");
        for (int i = 0; i < cinema.length; i++) {
            System.out.print(cinema[i] + ", ");
        }
        System.out.println();
    }

    static void displaySeatingArrangement() { //SeatingArrangement
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 10; j++) {
                System.out.print(seat[i][j] + " ");
            }
            System.out.println(" |");
        }
        System.out.print("      |");
        for (int i = 0; i < 6; i++) {
            System.out.print(" " + seatvip[i]);
        }
        System.out.println(" |");

    }

    static void bookSeats(String[] IDseat) { //bookSeats
        for (int i = 0; i < IDseat.length; i++) {
            int num = Character.getNumericValue(IDseat[i].charAt(1)) - 1;
            if (IDseat[i].charAt(0) != 'P') {
                if (IDseat[i].charAt(0) == 'A') {
                    seat[0][num] = "X";
                }
                if (IDseat[i].charAt(0) == 'B') {
                    seat[1][num] = "X";
                }
                if (IDseat[i].charAt(0) == 'C') {
                    seat[2][num] = "X";
                }
            } else {
                seatvip[num] = "X";
            }
        }
    }

    static void priceSeat(String[] IDseat) { //priceSeat
        int price;
        for (int i = 0; i < IDseat.length; i++) {
            if (IDseat[i].charAt(0) != 'P') {
                price = 150;
                priceseat = priceseat + price;
            } else {
                price = 300;
                priceseat = priceseat + price;
            }
        }
        System.out.println("The price of all tickets is " + priceseat);
    }
    
    static void displayMember() { //Member
        if (member == "1") {
            priceseat = (priceseat*100)/10 ;
        }else{
            priceseat = priceseat ;
        }
    }

    static void printTicket(String[] IDseat, String name, String tel, String mail, int numberCenima, int numberMovie) { //แสดงตั๋ว
        System.out.println(cinema[numberCenima]+" - "+movie[numberMovie]);
        System.out.println("   "+name + " " + tel + " " + mail);
        System.out.print("Your seat is ");
        for (int i = 0; i < IDseat.length; i++) {
            System.out.print(IDseat[i] + " ");
        }
        System.out.println();
        priceSeat(IDseat);

    }

}
